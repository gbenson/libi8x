What has changed in libi8x-python?
==================================

Changes since libi8x-python 0.0.1
---------------------------------

* py8x_xctx_call now properly considers data-type size when allocating
  memory for arguments and returns.
